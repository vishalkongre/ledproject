import * as firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyBSbHpBWUoqUGFAkVb_0XqK605nZWHIqXg",
  authDomain: "fir-js-dc942.firebaseapp.com",
  databaseURL: "https://fir-js-dc942.firebaseio.com",
  projectId: "fir-js-dc942",
  storageBucket: "fir-js-dc942.appspot.com",
  messagingSenderId: "95921251478",
  appId: "1:95921251478:web:86af507996072d1beb0eb7",
  measurementId: "G-18PHZQ5671",
};
firebase.initializeApp(config);
export default firebase;
