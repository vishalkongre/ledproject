import React from "react";
import "./App.css";
//Calling Bootstrap 4.5 css
import "bootstrap/dist/css/bootstrap.min.css";
//Calling Firebase config setting to call the data
import firebase from "./Firebase";
class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { studentslist: [] };
  }

  componentDidMount() {
    firebase
      .database()
      .ref("devices")
      .on("value", (snapshot) => {
        let studentlist = [];
        snapshot.forEach((snap) => {
          // snap.val() is the dictionary with all your keys/values from the 'students-list' path
          studentlist.push(snap.val());
        });
        this.setState({ studentslist: studentlist });
      });
  }

  render() {
    return (
      <div className="MainDiv">
        <div class="jumbotron text-center bg-sky">
          <h3>Devices data </h3>
        </div>

        <div className="container">
          <table id="example" class="display table">
            <thead class="thead-dark">
              <tr>
                <th>Id</th>
                <th>Fault</th>
                <th>lux</th>
                <th>loc</th>
              </tr>
            </thead>
            <tbody>
              {this.state.studentslist.map((data) => {
                return (
                  <tr>
                    <td>{data.id}</td>
                    <td>{data.fault}</td>
                    <td>{data.lux}</td>
                    <td>{data.loc}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
export default App;
